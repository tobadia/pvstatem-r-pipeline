# PvSTATEM Repository of R scripts and pipelines

This repository contains R code, along with (some) outputs, developped for the [PvSTATEM clinical trial](https://www.pvstatem.eu/). This README file is unfinished and will evolved over the upcoming days/weeks/years.

# Get R pipelines

Just clone this repository or download it as a zip file.

# `.env` usage

The `dotenv` package allows sourcing a text file (by default, named `.env` and located at the root of the R working directory) so that its content is interpreted as global variables by your system. These global variables can then be queried by R using the `Sys.getenv()` command. This method is especially useful for not including personal information in R scripts, such as credentials or API keys. These are extremely sensitive information that should never, ever, be shared with anyone.

A `.env.example` file is included, that contains placeholders for all the personal identifiers that arerequired to run the PvSTATEM R pipeline. The true `.env` file is **not** included in this repository, and it is also included in the list of files not tracked by git (see in [.gitignore](<.gitignore>)). 

Before any script can be ran, you should copy the `.env.example` file to create your own `.env` file and fill it with your pseronal data: 
```
cp .env.example .env
vi .env
```

# Description of projects and R pipelines

The [PvSTATEM R Pipeline.Rproj](<PvSTATEM R Pipeline.Rproj>) file is a generic RStudio project file that can be opened to serve as point of entry into the R pipelines.

This repository is structured in projects, each corresponding (in theory) to a an ad-hoc database collected as part of the PvSTATEM trial. All scripts and methods developed in project subdirectories apply only to their corresponding database.

## 00_COMMON

The `00_COMMON` project contains pipelines that were created before fieldwork started. This corresponds to the scripts used to generate the list of stickers for sample catalogue and participant identification. The outputs of these scripts are included as XLSX files, each row corresponding to the full content of a sticker.

## 01_INVENTORY

Set of methods used to explore and analyze the content of the **Population Inventory** databases hosted by partner institutions.

This project contains methods to transform the database from a wide format (used by REDCap for collecting demographics in different variables suffixed with numbers, each corresponding to distinct individuals) to a long format where a row correspond to a single participant. Once this conversion has been made, house-level and individual-leel operations can be performed, such as: 
* House-level: plot GPS coordinates of houses on top of maps/satellite views provided by third-party services such as Google Maps.
* Participant-level: explore population demographics (age, gender, number of participants per household/house/cluster...) and generate a sample from the list of inhabitants to generate a subset that wil be targeted for the observational study.

Generally speaking, you should not be sourcing or editing manually [INVENTORY_00_R_environment.R](<01_INVENTORY/INVENTORY_00_R_environment.R>), [INVENTORY_00_R_functions.R](<01_INVENTORY/INVENTORY_00_R_functions.R>) or [INVENTORY_01_dump_REDCap_database.R](<01_INVENTORY/INVENTORY_01_dump_REDCap_database.R>) as these scripts are sourced as part of the others, whenever it is necessary. The source database will only be extracted again if it is deemed too old (> 1 day old, that value being set in the `DATA_EXTRACT_EXPIRY_TIME_D`) flag.

### R environment setup and database extraction

#### Scripts involed

* [INVENTORY_00_R_environment.R](<01_INVENTORY/INVENTORY_00_R_environment.R>): load all required packages (or complain they are missing) and sets global variables for the 01_INVENTORY project.
* [INVENTORY_00_R_functions.R](<01_INVENTORY/INVENTORY_00_R_functions.R>): custom R functions developed for the 01_INVENTORY project.
* [INVENTORY_01_dump_REDCap_database.R](<01_INVENTORY/INVENTORY_01_dump_REDCap_database.R>): extract database from the REDCap server that is passed into the `RCON_INVENTORY` variable. Very minor tweaks are conducted on the data: manual GPS coordinates are propagated into the `gps_lat` and `gps_lon` fields, and a validity flag is creaated based on expected boundaries for GPS locations.

#### Outputs

* `dat_inventory_raw`: the raw database from REDCap with very minor tweaks

### Transform data into long format

#### Purpose

Convert the content of the raw REDCap database into a format that has individual in rows (data analysis) rather than columns (data collection).

#### Scripts involved

* [INVENTORY_02_list_all_inventory_participants.R](<01_INVENTORY/INVENTORY_02_list_all_inventory_participants.R>): list all persons from population inventory on individual rows.
* [INVENTORY_03_list_all_inventory_houses.R](<01_INVENTORY/INVENTORY_03_list_all_inventory_houses.R>): list all houses from population inventory in individual rows.

#### Outputs

* `inventory_list_p`: a table with individuals in rows and demographics in columns.
* `inventory_list_h`: a table with houses in rows and descriptors in columns (number of households, total number of people living in house, GPS coordinates).

### Map of houses

#### Purpose

Display summary demographics and plot map of house locations using their GPS coordinates.

#### Scripts involved

[INVENTORY_04_display_summary_demographics.R](<01_INVENTORY/INVENTORY_04_display_summary_demographics.R>): get cluster-level map backgrounds from external provider (set by the `MAP_PROVIDER` gobal variable, defaulting to Google Maps hybrid images (satellite + highlighted roads)) and overlay coordinates of houses on top.

#### Outputs

* `gps_data_range`: a list (country) of lists (clusters) providing extreme latitude and longitude values for each cluster.
* `gps_map_backgrounds`: a list (country) of lists (clusters) containing the map background from external provider that fits the content of `gps_data_range`.
* `gps_maps`: a list (country) of lists (clusters) containing the above maps on which GPS points are overlayed.
* `gps_map_panels`: a list (country) of lists (groups of clusters) with paneled version of `gps_maps` that is more convenient for plotting.

### Sampling for observational study

#### Purpose

To sample from the comprehensive list of population inventory and generate two distinct subsets: 

* `main`: the target sample for the observational study (N = 220, or total cluster size if below).
* `backup`: a secondary list consisting of additional participants that would still be (mostly) representative of the population demographics and could be used should the main list not be enough due to attrition, people not consenting...

**Note:** the process to generate these two mutually exclusive lists starts by sampling participants for the main list. Therefore, the main list is expected to be the most representative of population demographics. The backup list is generated by first removing from the pool of individuals all those that made it to the main list. Therefore, it may not contain enough individuals after stratification by age and sex to be fully representative of the original cluster population. This is a bias we'll have to live with.

#### Scripts involved

* [INVENTORY_05_generate_list_of_participants_for_observational_study.R](<01_INVENTORY/INVENTORY_05_generate_list_of_participants_for_observational_study.R>): extract a subset of participants in each cluster, while preserving the age-and-gender distribution observed in the population inventory.

#### Outputs

* `observational_list_p`: a table with individuals in rows and demographics in columns, to target for the observational study.

# Contribution policy

## General rules

All PvSTATEM partner are welcome to propose new contributions to this repository, providing they respect the following guidelines:

* The `main` branch may not be committed to except by repository maintainers.
* Never include any personal identifier in a commit (API key, credentials...). These should be processed through the `dotenv` package and a better description of that process is available above in that README.
* If you fork the repository, keep your branch up-to-date with origin so that pull-request won't result in trouble.
* Cloning the repository then developing your features in a dedicated branch to submit as a pull-request is the **preferred** way of contributing.
* Respect the naming conventions that follow:
  1. The root directory of the repo should only contain sub-directories starting with 2 digits, reflecting the order of that collection in the PvSTATEM study (these will be referred to as *projects*). The `00_COMMON` project and its content may be sourced from any other project, 
  2. Global variables should be project-dependent and stored in a file named `PROJECT_XX_R_environment.R`, where *PROJECT* is the name of the project and *XX* is its number (sequential, ascending),
  3. One script = one purpose. Do **not** write scripts containing pipelines related to multiple tasks as these are hard to maintain,
  4. Scripts outputs should mostly not be uploaded, but empty `output` folders are sync'd into the repository to preserve the expected structure of directories, 
  5. Preserve proper indentation, 
  6. Preserve overall code structure by favoring `tidyverse` syntax and `ggplot` plots over base R calls. Base R is acceptable but should be refactored into `tidyverse` syntax as soon as possible,
  7. Document all scripts and methods both inline (comments) and in this README.md (respecting the template just below)

## 02_OBSERVATIONAL

Set of methods used to explore and analyze the content of the **Observational Study** databases hosted by partner institutions.

### R environment setup and database extraction

#### Scripts involed

* [OBSERVATIONAL_00_R_environment.R](<02_OBSERVATIONAL/OBSERVATIONAL_00_R_environment.R>): load all required packages (or complain they are missing) and sets global variables for the 02_OBSERVATIONAL project.
* [OBSERVATIONAL_01_dump_REDCap_database.R](<02_OBSERVATIONAL/OBSERVATIONAL_01_dump_REDCap_database.R>): extract database from the REDCap server that is passed into the `RCON_OBSERVATIONAL` variable. 

#### Outputs

* `dat_observational_raw`: the raw database from REDCap.

## README.md template

The README.md should respect the following conventions:

```
# First-level header: reserved for general topics

## Second-level header: project subdirectory (except for this section)

### Third-level header: project milestone

```

For each project milestone, please preserve and document these three paragraphs: 

```
#### Purpose
Describe in a few words the aim of that milestone

#### Scripts involved
`[script_name](<path/to/file/script_name>)`: Explain what that script does

#### Outputs
* `output_item`: Describe what script_name has generated and kept in teh R environment
```

# Contacts

Repository maintainer: [Dr. Thomas OBADIA](mailto:thomas.obadia@pasteur.fr)  
Project lead: [Dr. Michael WHITE](mailto:michael.white@pasteur.fr)
