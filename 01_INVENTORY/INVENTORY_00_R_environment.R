## INVENTORY_00_R_environment.R
## Date     : 2024/01/26
## Author   : Thomas Obadia
##
## This is a general script for the 01_INVENTORY/ R environment, that
## will load all required packages into R and set global variables
## used by other scripts.
## 
## You should usually not source this file alone: it is sourced in the
## script that handles the REDCap dump.
######################################################################





######################################################################
### REQUIRED PACKAGES
######################################################################
require(dotenv)              # Easy handling personal global variables
require(redcapAPI)           # REDCap API methods
require(tidyverse)           # Data manipulation made easy
# require(OpenStreetMap)       # Set of methods for OpenStreetMap 
require(ggmap)               # More general set of methods





######################################################################
### SOURCE .env FILE
######################################################################
dotenv::load_dot_env("./01_INVENTORY/.env")





######################################################################
### DATA SOURCE TO BE USED
######################################################################
## Global flags that define which database should be dumped and which
## will be ignored. There is one flag per data source: IPP, IPM, AHRI.
#    - TRUE : data will be dumped
#    - FALSE: data will not be dumped
DATA_SOURCE_REDCAP_IPP_INV     <- FALSE
DATA_SOURCE_REDCAP_IPM_INV     <- TRUE
DATA_SOURCE_REDCAP_AHRI_INV    <- TRUE





######################################################################
### DATA UP-TO-DATE FLAG
######################################################################
## Global flag that defines if the database dump is recent enough or
## if a REDCap extract should be conducted again
#    - TRUE : data is recent enough, don't dump
#    - FALSE: new extract is necessary (default)
DATA_EXTRACT_IS_RECENT_INV     <- FALSE

## Date at which the last extract was done. Defaults to Epoch time
## in the R Environment file, so that a new dump is always done after
## sourcing this file.
DATA_EXTRACT_TS_DEFAULT    <- as.Date("1970-01-01")

## Number of days after which database should be extracted again
DATA_EXTRACT_EXPIRY_TIME_D <- 1





######################################################################
### GPS MAPPING
######################################################################
## Global flag to determine the kind of GPS data range.
#   - TRUE : from data (country-and-cluster level)
#   - FALSE: fixed (country level)
GPS_RANGE_FROM_COLLECTED_DATA <- TRUE

## Global flag to drop GPS points when considered not valid.
# Especially useful for when deriving GPS boundaries from data.
#   - TRUE : Drop invalid data
#   - FALSE: Keep invalid data (not recommended)
GPS_DROP_INVALID_POINTS       <- TRUE

## Min and max values admissible for GPS coordinates
GPS_LAT_MIN_E                 <-  07.250000    # >0 : South
GPS_LAT_MAX_E                 <-  07.650000    # >0 : South
GPS_LON_MIN_E                 <-  38.300000    # >0 : East
GPS_LON_MAX_E                 <-  38.800000    # >0 : East
GPS_LAT_MIN_M                 <- -19.850000    # <0 : North
GPS_LAT_MAX_M                 <- -19.350000    # <0 : North
GPS_LON_MIN_M                 <-  45.810000    # >0 : East
GPS_LON_MAX_M                 <-  46.310000    # >0 : East

## Offset values to avoid a GPS point being at the edge of a cluster
GPS_LAT_OFFSET                <- 0.004000
GPS_LON_OFFSET                <- 0.004000

## Map default provider
#    - "google": Google Maps. API key needed, satellite data available.
#    - "osm"   : OpenStreetMap. No API key needed, no satellite data.
MAP_PROVIDER                  <- "google"
if (MAP_PROVIDER == "google") {
  register_google(key = Sys.getenv("GOOGLE_API_KEY"))
}

## Map default type
#  Character string providing map theme. Options available are:
#  Google Maps: "terrain", "terrain-background", "satellite", "roadmap", "hybrid"
#  Stamen     : "terrain", "watercolor", and "toner"
MAP_TYPE                      <- "hybrid"

## Layout of panels for plotting GPS maps
MAP_PLOT_PANEL_LAYOUT_NROW    <- 3
MAP_PLOT_PANEL_LAYOUT_NCOL    <- 3





######################################################################
### SEED VALUE
######################################################################
## A global seed value to be used by set.seed() calls
SEED <- 12345
